package ro.ionutscheianu.client.main;

import ro.ionutscheianu.client.Client;

public class Main {
    public static void main(String[] args) {
        Client client = new Client();
        client.start();
    }
}