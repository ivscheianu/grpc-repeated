package ro.ionutscheianu.client;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import ro.ionutscheianu.generated.MatrixOrVectorOuterClass;
import ro.ionutscheianu.generated.ServiceGrpc;
import ro.ionutscheianu.generated.SumResponseOuterClass;
import ro.ionutscheianu.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class Client {

    private ManagedChannel managedChannel;

    public Client() {
        Properties properties = Utils.loadProperties();
        managedChannel = ManagedChannelBuilder.forAddress(properties.getProperty("ip"), Integer.parseInt(properties.getProperty("port"))).usePlaintext().build();
    }

    public void start() {
        List<Integer> list = getList();
        List<List<Integer>> matrix = getMatrix();
        getResultFromService(MatrixOrVectorFactory.getMatrixOrVector(list));
        getResultFromService(MatrixOrVectorFactory.getMatrixOrVector(matrix));
    }

    private void getResultFromService(MatrixOrVectorOuterClass.MatrixOrVector matrixOrVector) {
        try {
            int result = callService(matrixOrVector);
            System.out.println("Result = " + result);
        } catch (StatusRuntimeException statusRuntimeException) {
            System.out.println(statusRuntimeException.getMessage());
        }
    }

    private int callService(MatrixOrVectorOuterClass.MatrixOrVector matrixOrVector) {
        ServiceGrpc.ServiceBlockingStub divisionOperationServiceBlockingStub = ServiceGrpc.newBlockingStub(managedChannel);
        SumResponseOuterClass.SumResponse response = divisionOperationServiceBlockingStub.getSum(matrixOrVector);
        return response.getSumResult();
    }

    private List<List<Integer>> getMatrix() {
        List<Integer> list = getList();
        List<List<Integer>> matrix = new ArrayList<>();
        matrix.add(list);
        matrix.add(list);
        matrix.add(list);
        return matrix;
    }

    private List<Integer> getList() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(1);
        list.add(1);
        return list;
    }

}
