package ro.ionutscheianu.client;

import ro.ionutscheianu.generated.MatrixOrVectorOuterClass;

import java.util.List;

public final class MatrixOrVectorFactory {

    private MatrixOrVectorFactory() {

    }

    public static MatrixOrVectorOuterClass.MatrixOrVector getMatrixOrVector(List<?> list) {
        if (list.get(0) instanceof List<?>) {
            return createMatrix((List<List<Integer>>) list);
        }
        return createVector((List<Integer>) list);
    }

    private static MatrixOrVectorOuterClass.MatrixOrVector createMatrix(List<List<Integer>> matrix) {
        return getMatrixAtIndex(matrix, 0);
    }

    private static MatrixOrVectorOuterClass.MatrixOrVector getMatrixAtIndex(List<List<Integer>> matrix, int index) {
        MatrixOrVectorOuterClass.MatrixOrVector.Builder builder = MatrixOrVectorOuterClass.MatrixOrVector.newBuilder()
                .addAllNumbers(matrix.get(index));
        if (index < matrix.size() - 1) {
            builder.setMatrixOrVector(getMatrixAtIndex(matrix, index + 1));
        }
        return builder.build();
    }

    private static MatrixOrVectorOuterClass.MatrixOrVector createVector(List<Integer> list) {
        return MatrixOrVectorOuterClass.MatrixOrVector.newBuilder()
                .addAllNumbers(list)
                .build();
    }
}
