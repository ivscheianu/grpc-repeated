package ro.ionutscheianu.server.main;

import ro.ionutscheianu.server.Server;

public class Main {
    public static void main(String[] args) {
        Server server = new Server();
        server.start();
    }
}
