package ro.ionutscheianu.server;

import io.grpc.ServerBuilder;
import ro.ionutscheianu.server.service.Service;
import ro.ionutscheianu.utils.Utils;

import java.io.IOException;
import java.util.Properties;


public class Server {

    private io.grpc.Server server;

    public Server() {
        Properties properties = Utils.loadProperties();
        server = ServerBuilder.
                forPort(Integer.parseInt(properties.getProperty("port")))
                .addService(new Service())
                .build();
    }

    public void start(){
        System.out.println("Starting server...");
        try {
            server.start();
            System.out.println("Server started, port: " + server.getPort());
            server.awaitTermination();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
