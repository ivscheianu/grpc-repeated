package ro.ionutscheianu.server.service;

import io.grpc.stub.StreamObserver;
import ro.ionutscheianu.generated.MatrixOrVectorOuterClass;
import ro.ionutscheianu.generated.ServiceGrpc;
import ro.ionutscheianu.generated.SumResponseOuterClass;

import java.util.List;

public class Service extends ServiceGrpc.ServiceImplBase {
    @Override
    public void getSum(MatrixOrVectorOuterClass.MatrixOrVector request, StreamObserver<SumResponseOuterClass.SumResponse> responseObserver) {
        int sumResult = 0;
        sumResult = getSumResult(sumResult, request);
        SumResponseOuterClass.SumResponse.Builder emptyResponseBuilder = SumResponseOuterClass.SumResponse.newBuilder().setSumResult(sumResult);
        responseObserver.onNext(emptyResponseBuilder.build());
        responseObserver.onCompleted();
    }

    private int getSumResult(int sumResult, MatrixOrVectorOuterClass.MatrixOrVector matrixOrVector) {
        sumResult += getSumFromVector(matrixOrVector.getNumbersList());
        while (matrixOrVector.hasMatrixOrVector()) {
            sumResult += getSumFromVector(matrixOrVector.getNumbersList());
            matrixOrVector = matrixOrVector.getMatrixOrVector();
        }
        return sumResult;
    }

    private int getSumFromVector(List<Integer> list) {
        return list.parallelStream().mapToInt(Integer::intValue).sum();
    }
}
